package com.estebanbula.reativesecuredapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReativeSecuredAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReativeSecuredAppApplication.class, args);
	}

}
